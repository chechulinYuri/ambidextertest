﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bar : MonoBehaviour
{
    [SerializeField]
    RectTransform handler;

    public void setValue(float normalizedValue)
    {
        if (handler != null)
        {
            handler.anchorMax = new Vector2(normalizedValue, 1f);
            handler.offsetMax = Vector2.zero;
        }
    }
}
