﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FightSceneUI : MonoBehaviour
{
    static FightSceneUI instance;

    [SerializeField]
    Text statusField;

    private void Awake()
    {
        instance = this;
    }

    public static void SetStatus(string status)
    {
        if (instance != null)
        {
            instance.statusField.text = status;
        }
    }
}
