﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    [SerializeField]
    LayerMask clickableObjects;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 10000f, clickableObjects))
            {
                var clickable = hit.collider.GetComponent<IClickable>();

                if (clickable != null)
                {
                    clickable.onClick();
                }
            }
        }
    }
}

public interface IClickable
{
    void onClick();
}