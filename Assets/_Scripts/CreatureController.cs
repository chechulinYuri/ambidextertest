﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Component that controls the creature on the scene
/// It used Photon SDK to communicate between clients
/// </summary>
public class CreatureController : Photon.MonoBehaviour, IClickable
{
    // container for all creatures on the scene
    static Dictionary<int, CreatureController> allCreatures;

    // it used to know how many creatures on the scene
    public static int NextCreatureIndex
    {
        get
        {
            if (allCreatures != null)
            {
                return allCreatures.Count;
            }

            return 0;
        }
    }

    // link to the creature controller which is owned by the client
    public static CreatureController LocalPlayer;

    [Header("Creature settings")]
    [SerializeField]
    string golemName;

    // start health of the creature
    [SerializeField]
    float maxHealt = 100f;

    //
    [SerializeField]
    float cooldownAfterTakingDamage = 0.3f;

    //
    [SerializeField]
    float cooldownAfterDealingDamage = 0.8f;

    //
    [SerializeField]
    float damage = 10f;

    //
    [SerializeField]
    float criticalAttackChange = 0.35f;

    [Header("Animation settings")]
    [SerializeField]
    Animator animator;

    // number of attack animations in the animator
    [SerializeField]
    int nbOfAttackAnimations = 1;

    [SerializeField]
    ParticleSystem bloodParticle;

    [Header("UI settings")]
    [SerializeField]
    Bar healthBar;

    // current health of the creature
    float health;

    // time to wait before any action
    float actionTimeout;

    // mem variables to store attack data
    CreatureController memAttackTarget;
    float memAttackDamage;
    bool memCriticalAttack;

    // shows if creature dead or not
    protected bool isDead
    {
        get
        {
            return health <= 0f;
        }
    }

    // shows if creature can attack
    protected bool canAttack
    {
        get
        {
            return actionTimeout == 0f;
        }
    }

    private void Awake()
    {
        // create container for all creatures on the scene and add this creature
        if (allCreatures == null)
        {
            allCreatures = new Dictionary<int, CreatureController>();
        }

        allCreatures.Add(photonView.viewID, this);

        // assign health to maxHealth
        setHealth(maxHealt);

        // move creature to the creatures container
        // the container used by Kudan to simulate AR
        FightSceneController.PutCreatureInContainer(transform);
    }

    private void OnDestroy()
    {
        // remove creature from creatures container
        if (allCreatures != null && allCreatures.ContainsKey(photonView.viewID))
            allCreatures.Remove(photonView.viewID);
    }

    private void Start()
    {
        // if I am the owner of the network object
        if (photonView.isMine)
        {
            // assign local player obejct
            LocalPlayer = this;

            FightSceneUI.SetStatus(string.Format("You are controlling {0}", golemName));
        }
    }

    private void Update()
    {
        // update timeout if it is not 0
        if (!isDead && !canAttack)
        {
            actionTimeout = Mathf.Clamp(actionTimeout - Time.deltaTime, 0, actionTimeout);
        }
    }

    public void onClick()
    {
        // if you are not a spectator
        if (LocalPlayer != null)
        {
            // if we clicked on the enemy
            if (LocalPlayer != this)
            {
                // attack this creature
                LocalPlayer.attack(this);
            }
        }
    }

    public void attack(CreatureController target)
    {
        if (!isDead)
        {
            if (canAttack)
            {
                var isCritical = randomCriticalAttack();
                var attackAnimation = getRandomAttackAnimationIndex();
                var targetViewId = target.photonView.viewID;

                photonView.RPC("onAttack", PhotonTargets.All, targetViewId, damage, isCritical, attackAnimation);
            }
        }
    }

    [PunRPC]
    void onAttack(int targetViewId, float damage, bool isCritical, int attackAnimation)
    {
        var target = GetCreatureByOwnerId(targetViewId);

        if (target != null)
        {
            // look to the target
            var lookPos = target.transform.position - transform.position;
            lookPos.y = 0;
            var rotation = Quaternion.LookRotation(lookPos);
            transform.rotation = rotation;

            // play animation
            animator.SetInteger("AttackType", attackAnimation);
            animator.SetTrigger("Attack");

            // assign target
            memAttackTarget = target;
            memCriticalAttack = isCritical;

            // assign attack damage
            memAttackDamage = isCritical ? damage * 4f : damage;

            // update reload time
            if (actionTimeout < cooldownAfterDealingDamage)
            {
                actionTimeout = cooldownAfterDealingDamage;
            }
        }
    }

    public void applyDamage(float damage, bool isCritical)
    {
        if (!isDead)
        {
            // update health
            setHealth(Mathf.Clamp(health - damage, 0f, 100f));

            if (health == 0f)
            {
                animator.SetBool("IsDead", true);
            }
            else
            {
                animator.SetTrigger("TakeDamage");
            }

            // update reload timeout
            if (actionTimeout < cooldownAfterTakingDamage)
            {
                actionTimeout = cooldownAfterTakingDamage;
            }

            // play blood particles if attack is critical
            if (isCritical)
            {
                StartCoroutine(bloodCoroutine());
            }
        }
    }

    // shows blood particles and hides them after timeout
    IEnumerator bloodCoroutine()
    {
        bloodParticle.Play();

        yield return new WaitForSeconds(0.5f);

        bloodParticle.Stop();

    }

    // attack animations event
    // it calls from attack animations
    public void onApplyDamageAnimationEvent()
    {
        // apply damage to the target
        memAttackTarget.applyDamage(memAttackDamage, memCriticalAttack);
    }

    // set heaelth value and updates the health bar
    void setHealth(float value)
    {
        health = value;

        healthBar.setValue(health / maxHealt);
    }

    // returns random bool depending on the criticalAttackChange value
    bool randomCriticalAttack()
    {
        // get random value for critical attack chanse
        var criticalAttackRandom = Random.Range(0f, 1f);

        // if random value is in range between 0 and criticalAttackChange
        return criticalAttackRandom <= criticalAttackChange;
    }

    // returns random animation index for the attack
    int getRandomAttackAnimationIndex()
    {
        return Random.Range(0, nbOfAttackAnimations);
    }

    // returns creature by viewID
    public static CreatureController GetCreatureByOwnerId(int viewID)
    {
        if (allCreatures != null)
        {
            if (allCreatures.ContainsKey(viewID))
            {
                return allCreatures[viewID];
            }
        }

        return null;
    }
}
