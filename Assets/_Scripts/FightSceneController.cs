﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kudan.AR;
using Photon;
using ExitGames.UtilityScripts;

public class FightSceneController : PunBehaviour
{
    static FightSceneController instance;

    [SerializeField]
    KudanTracker tracker;

    [SerializeField]
    CreatureData[] creaturesData;

    [SerializeField]
    Transform creaturesContainer;

    bool isHeroCreated;

    private void Awake()
    {
        instance = this;
    }

    private IEnumerator Start()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);

            setupTrakerIfNeeded();
        }
    }

    void setupTrakerIfNeeded()
    {
#if !UNITY_EDITOR
        if (!tracker.ArbiTrackIsTracking())
        {
            // The current position in 3D space of the floor
            Vector3 floorPosition;

            // The current orientation of the floor in 3D space, relative to the device
            Quaternion floorOrientation;

            // Gets the position and orientation of the floor and assigns the referenced Vector3 and Quaternion those values
            tracker.FloorPlaceGetPose(out floorPosition, out floorOrientation);

            // Starts markerless tracking based upon the given floor position and orientations
            tracker.ArbiTrackStart(floorPosition, floorOrientation);
        }
#endif
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();

        StartCoroutine(waitBeforeSpawn());
    }

    IEnumerator waitBeforeSpawn()
    {
        yield return new WaitForSeconds(1f);

        // try to spawn hero for the client
        spawnCreature();
    }

    void spawnCreature()
    {
        int myIndex = CreatureController.NextCreatureIndex;

        Debug.Log("On join room " + myIndex);

        // if hero is not created yet
        if (!isHeroCreated)
        {
            // if index is less than golems number
            if (myIndex < creaturesData.Length)
            {
                var creatureName = creaturesData[myIndex].creaturePrefab.name;
                var spawnPosition = creaturesData[myIndex].spawn.position;
                var rotation = creaturesData[myIndex].spawn.rotation;

                // spawn new hero object
                PhotonNetwork.Instantiate(creatureName, spawnPosition, rotation, 0);

                isHeroCreated = true;
            }
        }
    }

    public static void PutCreatureInContainer(Transform creature)
    {
        if (instance != null)
        {
            creature.SetParent(instance.creaturesContainer);
        }
    }
}

[System.Serializable]
public class CreatureData
{
    public Transform spawn;
    public CreatureController creaturePrefab;
}